Manage the photo album
========================


## 1. clone the main folder

```
git clone https://gitlab.com/bakerwm/hugo_album.git
```

## 2. Configuration


`/config.toml`


```
# Settings
baseurl = "/dev/"
languageCode = "en-us"
title = "Photo"
theme = "phugo"

```

Change `baseurl` to "/" or "www.<your_website>.com" in your case. in this template `/dev/`, I would use the website under a subfolder using Apachd2.

**To-do list**

 - [ ] deploy the album and main site on github  
 - [ ] Create a script to parse description file for all the photos


## 3. Add content

`hugo new <name_of_album>/_index.md`

Then update the `_index.md` file:

```
+++
title = "Science"
date  = 2018-01-21T08:13:29+08:00
albumthumb = "/dev/images/K2_2006b.jpg"
+++


{{< photo full="/dev/images/K2_2006b.jpg" thumb="/dev/images/K2_2006b.jpg" alt="" phototitle="K2" description="K2 (乔戈里峰), also known as Mount Godwin-Austen or Chhogori, is the second highest mountain in the world (8611m). (source: https://en.wikipedia.org/wiki/K2) ">}}

```


 - [ ] change the `title`  
 - [ ] add `thumb`, `full`, `phototitle`, `description` 