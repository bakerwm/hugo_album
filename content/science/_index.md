+++
title = "Science/"
date  = 2018-01-21T08:13:29+08:00
albumthumb = "/photo/images/K2_2006b.jpg"
+++


{{< photo full="/photo/images/K2_2006b.jpg" thumb="/photo/images/K2_2006b.jpg" alt="" phototitle="K2" description="K2 (乔戈里峰), also known as Mount Godwin-Austen or Chhogori, is the second highest mountain in the world (8611m). (source: https://en.wikipedia.org/wiki/K2) ">}}
