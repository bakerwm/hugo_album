+++
title = "{{ replace .Dir "-" " " | title }}"
date  = {{ .Date }}
albumthumb = "/dev/images/K2_2006b.jpg"
+++


{{< photo full="/dev/images/K2_2006b.jpg" thumb="/dev/images/K2_2006b.jpg" alt="" phototitle="K2" description="K2 (乔戈里峰), also known as Mount Godwin-Austen or Chhogori, is the second highest mountain in the world (8611m). (source: https://en.wikipedia.org/wiki/K2) ">}}
